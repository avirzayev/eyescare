
const int trig = 10;
const int echo = 9;
const double soundSpeed = 0.034;

void setup() {
  // put your setup code here, to run once:
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(getDistance());
}

int getDistance(){
  digitalWrite(trig,LOW);
  delayMicroseconds(2);

  digitalWrite(trig,HIGH);
  delayMicroseconds(10);
  digitalWrite(trig,LOW);

  int duaration = pulseIn(echo, HIGH);

  return duaration*soundSpeed/2;
}
