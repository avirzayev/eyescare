import serial
import pygame
import os
import time
from ctypes import cast, POINTER
from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume

# establish connection
s = serial.Serial("COM7", 9600)

# while the person is away from the screen
while True:
    try:
        # get the distance from the arduino
        distance = int(s.readline())
        print(distance)

        # break if the person is too close
        if 17 >= distance > 0:
            break
    except:
        pass


devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(
   IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))

volume.SetMasterVolumeLevel(-0.0, None)  # setting the volume to max

pygame.init()  # initializing pygame

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)  # fullscreen
done = False

w, h = pygame.display.get_surface().get_size()

jumpscareIMG = pygame.image.load('jumpscare.jpg')  # loading the image
jumpscareIMG = pygame.transform.scale(jumpscareIMG, (w, h)) # scaling to fit it to the screen

pygame.mixer.music.load('sound.mp3')  # loading the sound
pygame.mixer.music.play(0)  # playing it

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    screen.blit(jumpscareIMG, (0, 0))  # draw the image

    pygame.display.flip()

    time.sleep(5)  # wait 5 seconds

    os.system("shutdown -s -t 0")  # shut down the pc immediately
    break






